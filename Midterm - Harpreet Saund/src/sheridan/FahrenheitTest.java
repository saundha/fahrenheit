package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 * @author Harpreet Saund
 *
 */
public class FahrenheitTest {

	@Test
	public void fahrenheitFromCelsiusTestRegular() {
		assertTrue("Invalid input.", Fahrenheit.fromCelsius(100) == 212 );
	}
	
	@Test(expected = NumberFormatException.class)
	public void fahrenheitFromCelsiusTestException() {
		int fahrenheit = Fahrenheit.fromCelsius(-222);
		fail("Number cannot be negative");
	}
	
	@Test
	public void fahrenheitFromCelsiusTestBoundryIn() {
		assertTrue("Invalid input.", Fahrenheit.fromCelsius(1) == 34 );
	}
	
	@Test(expected = NumberFormatException.class)
	public void fahrenheitFromCelsiusTestBoundryOut() {
		assertFalse("Invalid input.", Fahrenheit.fromCelsius(-1) == 212 );
	}

}
