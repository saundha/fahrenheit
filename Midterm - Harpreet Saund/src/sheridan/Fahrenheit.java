package sheridan;

public class Fahrenheit {

	public static int fromCelsius(int temp) {
		if (temp < 0)
			throw new NumberFormatException();
		
		float F = temp * (9f / 5) + 32;
		return Math.round(F);
	}

}
